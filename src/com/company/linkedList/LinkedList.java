package com.company.linkedList;

public class LinkedList<T> {

    Node<T> head;
    Node<T> tail;
    long size = 0L;

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        }
        return false;
    }

    public void addFirst(T data) {
        if (this.head == null) {
            addLast(data);
        } else {
            Node newNode = new Node(data);
            this.head.previous = newNode;
            newNode.next = this.head;
            this.head = newNode;
            this.size++;
        }
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getTail() {
        return tail;
    }

    public void addLast(T data) {
        Node<T> newNode = new Node<>(data);
        if (this.head == null) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            newNode.previous = this.tail;
            this.tail.next = newNode;
            this.tail = newNode;
            if (this.head.next == null) {
                this.head.next = newNode;
            }
        }
        this.size++;
    }


    public T removeFirst() {
        if (this.head != null) {
            T data = this.head.data;
            this.head = this.head.next;
            if (this.head != null) {
                this.head.previous = null;
            } else {
                this.tail = null;
            }
            this.size--;
            return data;
        }
        return null;
    }

    public long size() {
        return size;
    }


}
