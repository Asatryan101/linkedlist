package com.company;

import com.company.linkedList.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.addFirst(5);
        list.removeFirst();
        list.addFirst(6);
        list.addLast(8);
        list.addLast(7);
        System.out.println("size:" + list.size());
        System.out.println("head:" + list.getHead());
        System.out.println("tail:" + list.getTail());

    }
}
